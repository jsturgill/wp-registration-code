To use the plugin, place the "user_registration_codes" directory in your plugins
directory.  Feel free to change the name of the directory.

Open config.php to set various flags and values to your liking.  Explanations
for each setting are supplied in the config file.

Edit codes.php to enter your registration codes and the roles that will be
granted to any user who registers with that code.

The plugin supports Buddypress out of the box.  If you are not using Buddypress, 
it is still simple to integrate the plugin.  Call "draw_registration_code_field" 
in an action or edit the php file that controls your registration form to 
include the following:

<?php draw_registration_code_field();?>

wherever you would like the field to appear.