<?php
/*
Plugin Name: Registration Code
Plugin URI: http://jeremiahsturgill.com
Description: Uses optional registration codes to set new users' roles when they sign up.  Add the following php to your registration form, or call the function in an action: <strong>&lt;?php if ( function_exists( 'draw_registration_code_field' ) ) draw_registration_code_field(); ?&gt;</strong>
Author: Jeremiah Sturgill
Version: 1.1
Author URI: http://jeremiahsturgill.com
*/

define( REG_CODE_PLUGIN_BASE_PATH , plugin_dir_path(__FILE__) );

include( REG_CODE_PLUGIN_BASE_PATH . 'config.php' );

function process_registration_code( $user_id )
{
  include ( REG_CODE_PLUGIN_BASE_PATH . 'codes.php' ); 
  
  // Whitespace is removed from the code submitted by the user.  That is the 
  // extend the code is modified unless the case insensitive flag is set to
  // TRUE.
  $reg_code = trim( (string)$_POST[REG_CODE_FIELD_NAME] );
  
  if ( CASE_INSENSITIVE_REG_CODES === TRUE )
  {
    $new_map = array();
    foreach(  $code_to_role_map as $code=>$role)
    {
      $new_map[strtolower($code)] = $role;
    }
    $reg_code = strtolower( $reg_code );
    $code_to_role_map = $new_map;
  }
  
  if( $reg_code !== '' && array_key_exists( $reg_code, $code_to_role_map ) )
  {
    
    $wp_user_object = new WP_User( $user_id );
    if ( REPLACE_DEFAULT_REG_ROLE === TRUE )
    {
      $wp_user_object->set_role( $code_to_role_map[ $reg_code ] );
    } else {
      $wp_user_object->add_role( $code_to_role_map[ $reg_code ] );
    }
  }
}

// This function can be inserted in a theme or called from an action hook
// in order to insert the registration code field in your site.
function draw_registration_code_field()
{
  $value = ( KEEP_REG_FIELD_VALUE_ON_ERROR === TRUE ) ? 
           $_POST[ REG_CODE_FIELD_NAME ] : '';
  
  echo '
    <div class="' . REG_CODE_FIELD_NAME . '_holder" 
      style="' . REG_CODE_HOLDER_STYLES . '">
        <label class="' . REG_CODE_FIELD_NAME . '_label" 
          for="' . REG_CODE_FIELD_NAME .'">Registration Code (optional)</label>
        <input type="text" id="' . REG_CODE_FIELD_NAME . '" 
          name="' . REG_CODE_FIELD_NAME . '" value="' . $value . '"/>
    </div>';
}

if ( INTEGRATE_REG_CODE_WITH_BUDDYPRESS === TRUE )
{
  add_action(BUDDYPRESS_REG_CODE_ACTION, 'draw_registration_code_field');
}

// The line that makes the magic happen:
add_action('user_register', 'process_registration_code');