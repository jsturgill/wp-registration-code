<?php
// Note: set up your registration codes by editing the array located in
// codes.php.

// Used to generate the field name, ID, label class, and holder class.
define( 'REG_CODE_FIELD_NAME', 'signup_code' );

// Set to TRUE to display the submitted value if the registration form
// hiccups for any reason (e.g., the passwords don't match, or a required
// form field was omitted).
define( 'KEEP_REG_FIELD_VALUE_ON_ERROR', TRUE );

// Set to false for case-sensitive registration codes.
// Case Sensitive: "THISCODE" is not the same as "ThisCode"
// Case Insensitive: "THISCODE" is exactly the same as "ThisCode"
define( 'CASE_INSENSITIVE_REG_CODES', TRUE );

// Setting this to TRUE means new users with valid codes will have only one role
// on the site: that defined by the code.  Setting it to false means the user
// will have two roles:  the default role, and the role assigned by the code.
define( 'REPLACE_DEFAULT_REG_ROLE', TRUE );

// Styles here will be applied to the div that contains the reg field.
// Change to '' to omit styles.
define( 'REG_CODE_HOLDER_STYLES', 'clear:both;width:200px;padding-top:.5em;' );

// Setting this to true will automagically draw the field in the Buddypress 
// registration form.
define( 'INTEGRATE_REG_CODE_WITH_BUDDYPRESS', TRUE );

define( 'BUDDYPRESS_REG_CODE_ACTION', 'bp_after_signup_profile_fields');